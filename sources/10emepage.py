# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 14:20:32 2024

@author: jschmittze
"""

#import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> jeu pirate  </title>
    <link id="css"  rel="stylesheet" type="text/css" href="styles/pirate3.css">
</head>
<body>

    	<h1> THE LOST TREASURE </h1>
        </br>
        <p>Tu te trouves dès à présent face à deux portes. Qu'est-ce qu'il y a derrière ? Ca te tente de savoir ? Attention, La première porte mène aux trésors de plus de 100 000 pièces d'or cependant la Marine t'as rattrapée, tu vas devoir te battre... En outre, une des portes reste mystérieuse, elle révèle un autre trésor tellement rare, tellement incroyable, ta soif de curiosité te demande d'ouvrir cette porte.</p>
        </br>
        <p>Alors, que décides-tu ?<p/>
        <img src="../images/pirate.png" class="pirate" alt="Godric"/>
        </br> </br>
        <button class="chat" onclick="window.location.href='11emepage1.py';">Choisir la première porte </button>
        <button class="or" onclick="window.location.href='11emepage.py';">Choisir cette mystérieuse porte</button>
        
</body>
</html>"""

print(html)