# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 19:50:06 2024

@author: jschmittze
"""

#import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> jeu pirate  </title>
    <link id="css"  rel="stylesheet" type="text/css" href="styles/pirate3.css">
</head>
<body>

    	<h1> THE LOST TREASURE </h1>
        </br>
        <p>Tu es ce genre de personne à faire confiance aux inconnus ? Tu as bien fais, maintenant tu vas pouvoir continuer ta quête sans perdre de temps à dormir. </p>
        </br>
        <p>Tu es maintenant face à un lac, une chaloupe est juste à côté. Fais-tu le choix de prendre cette chaloupe afin de traverser le lac, au risque de tomber sur la marine car eux aussi peuvent passer par le lac, ou alors tu préfères jouer la sécurité et contourner le lac et passer par la forêt au risque de perdre du temps ?  </p>
        <p> <p/>
        <p><img src="../images/chaloupe.png" class="chaloupe" alt="Chaloupe"/></p>
        <p><img src="../images/foret.png" class="foret" alt="Forêt"/></p>
        <img src="../images/pirate.png" class="pirate" alt="Godric"/>
        </br> </br>
        <button class="chaloupe" onclick="window.location.href='4emepage.py';">Traverser le lac en chaloupe </button>
        <button class="foret1" onclick="window.location.href='4emepagebis.py';">Traverser la forêt à pied </button>
</body>
</html>"""

print(html)