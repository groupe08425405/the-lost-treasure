# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 14:11:28 2024

@author: jschmittze
"""

#import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> jeu pirate  </title>
    <link id="css"  rel="stylesheet" type="text/css" href="styles/pirate3.css">
</head>
<body>

    	<h1> THE LOST TREASURE </h1>
        </br>
        <p>Bravo ! Tu as réussi à traverser le lac et es arrivé sain et sauf ! Et en plus de cela, tu n'as pas croisé la Marine !  </p>
        <p>Youhou !!!</p>
        <p> <p/>
        <img src="../images/pirate.png" class="pirate" alt="Godric"/>
        </br> </br>
        <button class="continue" onclick="window.location.href='5emepage.py';">Continuer l'aventure !</button>
        
</body>
</html>"""

print(html)