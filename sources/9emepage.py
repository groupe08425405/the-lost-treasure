# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 14:13:19 2024

@author: jschmittze
"""

#import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> jeu pirate  </title>
    <link id="css"  rel="stylesheet" type="text/css" href="styles/pirate3.css">
</head>
<body>

    	<h1> THE LOST TREASURE</h1>
        </br>
        <p>Tu arrives à destination, la carte en est formelle.</p>
        </br>
        <p>Mais, évidemment, la quête n'est pas à son terme... un dilemme fera à nouveau surface...<p/>
        <img src="../images/pirate.png" class="pirate" alt="Godric"/>
        </br> </br>
        <button class="continue" onclick="window.location.href='10emepage.py';">Continue ton chemin</button>
        
</body>
</html>"""

print(html)