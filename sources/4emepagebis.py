# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 19:06:18 2024

@author: Margot
"""


#import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> jeu pirate  </title>
    <link id="css"  rel="stylesheet" type="text/css" href="styles/pirate3.css">
</head>
<body>

    	<h1> THE LOST TREASURE </h1>
        </br>
        <p>Tu as choisis de passer par la forêt,tu as peut-être éviter la Marine!  </p>
        <p>Tu es maintenant dans une grande forêt plutôt lumineuse elle ne parait pas si effrayante que ça.</p>
        <p>Tu voit au loin un sac,en t'approchant tu constate qu'il est remplie d'or.<p/>
        <p>Tu hésite soit tu le garde et part ou tu cherche son propriétaire.</p>
        <img src="../images/pirate.png" class="pirate" alt="Godric"/>
        </br> </br>
        <button class="voler" onclick="window.location.href='5emepage.py';">Prendre le sac et fuir</button>
        <button class="rendre" onclick="window.location.href='3emegameover.py';">Chercher le propriétaire</button>
        
</body>
</html>"""

print(html)