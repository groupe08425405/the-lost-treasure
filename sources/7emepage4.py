# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 14:07:28 2024

@author: mpeyramaur
"""



#import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> jeu pirate  </title>
    <link id="css"  rel="stylesheet" type="text/css" href="styles/pirate3.css">
</head>
<body>

    	<h1> THE LOST TREASURE </h1>
        </br>
        <p>Tu as trouvé un autre endroit pour camper.</p>
        <p>Mais les ténèbres se sont installés et pendant 3 jours tu ne v'y pas un seul rayon de soleil.</p>
        <p>Ton esprit se trouble de plus en plus,tu n'arrive plus à trouver ton chemin vers le trésor.</p>
        
        <img src="../images/pirate.png" class="pirate" alt="Godric"/>
        </br> </br>
        
        <button class="continue" onclick="window.location.href='5emegameover.py';">Tu continue ton chemin</button>
        
</body>
</html>"""

print(html)