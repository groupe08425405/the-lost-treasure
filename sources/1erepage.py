# -*- coding: utf-8 -*-
"""
Created on Sun Feb  4 15:15:38 2024

@author: Margot
"""
#import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> jeu pirate  </title>
    <link id="css"  rel="stylesheet" type="text/css" href="styles/pirate3.css">
</head>
<body>
    
    	<h1> THE LOST TREASURE </h1>
        </br>
        <p>Tu as trouvé une carte!</p>
        <p><img src="../images/carte.png" class="carte"/></p>
        
        <p class="option3"> Tu peux suivre le chemin rouge, </br>c'est le plus rapide mais attention la Marine peut avoir la meme carte! </p>
        <button class="option3" onclick="window.location.href='1ergameover.py';">Suivre le premier chemin </button>
       
        <p class="option2">Tu peux sinon suivre ton propre chemin en commençant par la ville d'Idoledo. </p>
        <button class="option4" onclick="window.location.href='2emepage.py';">Suivre le deuxieme chemin </button>
        <p><img src="../images/pirate.png" class="pirate" alt="Godric"/></p>
        
       
</html>"""

print(html)