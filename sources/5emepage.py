# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 14:30:45 2024

@author: jschmittze
"""


#import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> jeu pirate  </title>
    <link id="css"  rel="stylesheet" type="text/css" href="styles/pirate3.css">
</head>
<body>

    	<h1> THE LOST TREASURE </h1>
        </br>
        <p>Tu vois un groupe de pirate au loin qui te font signe.</p>
        <p>Tu ne sais pas si tu peux leurs faire confiance.</p>
        <p> <p/>
        <img src="../images/groupe_pirates.png" class="groupe" alt="groupe"/>
        <img src="../images/pirate.png" class="pirate" alt="Godric"/>
        </br> </br>
        <button class="confiance" onclick="window.location.href='4emegameover.py';">Tu choisis de faire équipe</button>
        <button class="pasconfiance" onclick="window.location.href='6emepage.py';">Tu continue ta route seul</button>
</body>
</html>"""

print(html)