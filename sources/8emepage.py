# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 14:02:43 2024

@author: jschmittze
"""

#import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> jeu pirate  </title>
    <link id="css"  rel="stylesheet" type="text/css" href="styles/pirate3.css">
</head>
<body>

    	<h1> THE LOST TREASURE </h1>
        </br>
        <p>Tu as bien fais !! Heureusement que tu es entré vénérer ces Dieux ! </p>
        </br>
        <p>Maintenant, grâce à la potion que ce fameux marchant t'as donné, tu continues ton chemin pendant trois jours ! Tu as donc dès à présent deux jours d'avance sur la Marine ! <p/>
        <img src="../images/pirate.png" class="pirate" alt="Godric"/>
        </br> </br>
        <button class="continue" onclick="window.location.href='9emepage.py';">Continue ton chemin</button>
        
</body>
</html>"""

print(html)