# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 14:07:17 2024

@author: mpeyramaur
"""



#import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> jeu pirate  </title>
    <link id="css"  rel="stylesheet" type="text/css" href="styles/pirate3.css">
</head>
<body>

    	<h1> THE LOST TREASURE </h1>
        </br>
        <p>Tu trouve un vallon dégagé pour installer ton campement.</p>
        <p>Il y a une jolie petite rivière tu décide d'y aller te raffraichir. </p>
        <p>Mais arrivé dedans elle devient rouge sang comme si tous les poisson et animaux s'étaient tous vidés de leurs sang.</p>
        <p>Tu sors de la rivière et cours chercher un autre endroit pour ton campement.</p>
        <img src="../images/pirate.png" class="pirate" alt="Godric"/>
        </br> </br>
        
        <button class="continue" onclick="window.location.href='7emepage4.py';">Tu continue ton chemin</button>
        
</body>
</html>"""

print(html)